import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class mpecdsaDemo {

    /**
     * This method generates public and private key, saves private key with other setup info and returns public key
     * @param ip string containing IP addresses in respective order separated by commas e.g. "123.123.123.123,0.0.0.0"
     * @param port port for communication, in case of more than 2 parties this is the lowest port and port + 1, port + 2, etc. will be used for other pairs of parties
     * @param parties number of parties participating
     * @param index index of party (the lowest index is 0)
     * @param threshold size of threshold
     * @param path path for a setup file
     * @return raw public key
     */
    public static native byte[] mpSetup(String ip, int port, int parties, int index, int threshold, String path);

    /**
     * This method is used for creating signature.
     * @param ip string containing IP addresses in respective order separated by commas e.g. "123.123.123.123,0.0.0.0"
     * @param port port for communication, in case of more than 2 parties this is the lowest port and port + 1, port + 2, etc. will be used for other pairs of parties
     * @param indices string of indices of parties participating in the signing process e.g. "0,2,5" for 2 out of 2 case it's always "0,1"
     * @param path path of the setup file
     * @param data_hash byte array with hash of the data to be signed
     * @return raw signature
     */
    public static native byte[] mpSign(String ip, int port, String indices, String path, byte[] data_hash);


    /**
     * setup string_of_ip_addresses port party_index /path/for/setup.file /path/for/publicKey.pem
     * e.g. setup "0.0.0.0,123.123.123.123" 1234 0 /path/for/setup.file /path/for/publicKey.pem
     *
     * sign string_of_ip_addresses port /path/of/setup.file /path/for/signature /path/of/file/to/be/signed/file.f
     * e.g. sign "0.0.0.0,123.123.123.123" 1234 /path/of/setup.file /path/for/signature /path/of/file/to/be/signed/file.f
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        File library = new File("/path/to/libmpecdsa.so");
        System.load(library.getAbsolutePath());

        switch (args[0]) {
            case "setup":
                byte[] pk = mpSetup(args[1], Integer.parseInt(args[2]), 2, Integer.parseInt(args[3]), 2, args[4]);
                writePkPem(args[5], pk);
                System.out.println("Setup finished");
                break;

            case "sign":
                byte[] fileContent = Files.readAllBytes(Paths.get(args[5]));
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                fileContent = digest.digest(fileContent);
                byte[] signature = mpSign(args[1], Integer.parseInt(args[2]), "0,1", args[3], fileContent);
                FileOutputStream stream = new FileOutputStream(args[4]);
                stream.write(formatSignature(signature));
                stream.close();
                System.out.println("Signing finished");
                break;
        }
    }

    /**
     * Takes raw secp256k1 ECDSA public key converts it to pem format and saves it to a file
     * @param path path for the signature file
     * @param pubk raw public key
     * @throws FileNotFoundException
     */
    static void writePkPem(String path, byte[] pubk) throws FileNotFoundException {
        byte[] head = Base64.getDecoder().decode("MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAE".getBytes());
        byte[] pk = new byte[88];
        System.arraycopy(head, 0, pk, 0, 24);
        System.arraycopy(pubk, 0, pk, 24, 64);

        PrintWriter out = new PrintWriter(path);
        out.println("-----BEGIN PUBLIC KEY-----");
        out.println(new String(Base64.getEncoder().encode(pk)));
        out.println("-----END PUBLIC KEY-----");
        out.close();

    }

    /**
     * transforms raw ECDSA signature to DER format
     * @param signature raw (r,s) ECDSA signature
     * @return DER formatted ECDSA signature
     */
    public static byte[] formatSignature(byte[] signature) {
        int rFirstBit = (byteToUnsignedInt(signature[0])) > 0x7f ? 1 : 0; // value of first bit of r
        int sFirstBit = (byteToUnsignedInt(signature[32])) > 0x7f ? 1 : 0; // value of first bit of s
        byte[] result = new byte[64 + 4 + rFirstBit + sFirstBit + 2];
        result[0] = ((byte) 0x30);
        result[1] = ((byte) (64 + 4 + rFirstBit + sFirstBit)); // remaining data size
        result[2] = ((byte) 0x02);
        result[3] = ((byte) (32 + rFirstBit)); // length of r
        int pointer = 4;
        if (rFirstBit == 1) {
            result[pointer] = ((byte) 0x00);
            pointer++;
        }
        for (int i = 0; i < 32; i++) {
            result[pointer] = (signature[i]);
            pointer++;
        }
        result[pointer] = ((byte) 0x02);
        pointer++;
        result[pointer] = ((byte) (32 + sFirstBit)); // length of s
        pointer++;
        if (sFirstBit == 1) {
            result[pointer] = ((byte) 0x00);
            pointer++;
        }
        for (int i = 32; i < 64; i++) {
            result[pointer] = (signature[i]);
            pointer++;
        }
        return result;
    }

    public static int byteToUnsignedInt(byte b) {
        return b & 0xff;
    }
}
