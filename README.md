# mpecdsaDemo


## How to run this demo

###### 1. Clone this repository
```
$ git clone https://gitlab.com/jirigav/mpecdsademo.git
```

###### 2. Clone Multiparty ECDSA library and compile it
Clone repository and initialize submodules:
```
$ git clone https://gitlab.com/jirigav/mpecdsa.git
$ cd ./mpecdsa
$ git submodule init; git submodule update
```
Install Rust:
```
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Set Rust to nightly version:
```
$ rustup default nightly
```

Compile the library:
```
$ cargo build --release
```

###### 3. Set path of the compiled library file in mpecdsaDemo.java at line 46

###### 4. Compile and run the demo
```
$ javac mpecdsaDemo.java

$ java mpecdsaDemo setup string_of_ip_addresses port party_index /path/for/setup.file /path/for/publicKey.pem
e.g. java mpecdsaDemo setup "0.0.0.0,123.123.123.123" 1234 0 /path/for/setup.file /path/for/publicKey.pem

$ java mpecdsaDemo sign string_of_ip_addresses port /path/of/setup.file /path/for/signature /path/of/file/to/be/signed/file.f
e.g. java mpecdsaDemo sign "0.0.0.0,123.123.123.123" 1234 /path/of/setup.file /path/for/signature /path/of/file/to/be/signed/file.f
```

##### How to compile the library for android
see https://medium.com/visly/rust-on-android-19f34a2fb43
